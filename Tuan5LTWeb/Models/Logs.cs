﻿namespace Tuan5LTWeb.Models
{
	public class Logs
	{
		public int ID { get; set; }
		public Transactions? Transactions { get; set; }
		public DateTime LogDate { get; set; }
		public DateTime LogTime { get; set; }
		public string AddTextHere { get; set; }
	}
}
