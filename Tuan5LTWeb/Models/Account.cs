﻿namespace Tuan5LTWeb.Models
{
	public class Account
	{
		public int ID { get; set; }
		public Customer? Customer { get; set; }
		public string AccountName { get; set; }
	}
}
