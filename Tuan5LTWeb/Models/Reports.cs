﻿namespace Tuan5LTWeb.Models
{
	public class Reports
	{
		public int ID { get; set; }
		public Account? Account { get; set; }
		public Logs? Logs { get; set; }
		public Transactions? Transactional { get; set; }
		public string ReportName { get; set; }
		public DateTime ReportDate { get; set; }
	}
}
