﻿namespace Tuan5LTWeb.Models
{
	public class Transactions
	{
		public int ID { get; set; }
		public Employees? Employees { get; set; }
		public Customer? Customer { get; set; }
		public string Name { get; set; }
	}
}
