﻿using Microsoft.EntityFrameworkCore;

namespace Tuan5LTWeb.Models
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Logs> Logs { get; set; }
        public DbSet<Transactions> Transaction { get; set; }
        public DbSet<Reports> Report { get; set; }
        public DbSet<Employees> Employee { get; set; }

    }
}
